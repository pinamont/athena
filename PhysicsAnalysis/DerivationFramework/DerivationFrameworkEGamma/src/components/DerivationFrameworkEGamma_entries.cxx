#include "DerivationFrameworkEGamma/PhotonsDirectionTool.h"
#include "DerivationFrameworkEGamma/EGammaPassSelectionWrapper.h"
#include "DerivationFrameworkEGamma/EGInvariantMassTool.h"
#include "DerivationFrameworkEGamma/EGTransverseMassTool.h"
#include "DerivationFrameworkEGamma/EGSelectionToolWrapper.h"
#include "DerivationFrameworkEGamma/BkgElectronClassification.h"


using namespace DerivationFramework; 
DECLARE_COMPONENT( PhotonsDirectionTool )
DECLARE_COMPONENT( EGammaPassSelectionWrapper )
DECLARE_COMPONENT( EGInvariantMassTool )
DECLARE_COMPONENT( EGTransverseMassTool )
DECLARE_COMPONENT( EGSelectionToolWrapper )
DECLARE_COMPONENT( BkgElectronClassification )

 

